FROM php:8.1-apache

# Install dependencies
RUN apt-get update && \
    apt-get install -y \
    libpq-dev \
    libzip-dev \
    unzip \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install PHP extensions
RUN docker-php-ext-install pdo pdo_pgsql zip

# Enable Apache modules
RUN a2enmod rewrite

# Set up working directory
WORKDIR /var/www/html

# Copy application files to container
COPY . /var/www/html

# Set folder permissions
RUN chown -R www-data:www-data /var/www/html/storage /var/www/html/bootstrap/cache

RUN echo "memory_limit=-1" > /usr/local/etc/php/conf.d/memory-limit.ini


# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#RUN composer config repo.packagist composer https://packagist.org


# Install application dependencies
RUN composer install --no-dev

# Set up environment variables
ENV APP_ENV=production
ENV APP_DEBUG=false

# Expose port 80
EXPOSE 80

# Set entrypoint
ENTRYPOINT ["php", "artisan", "serve", "--host=0.0.0.0", "--port=80"]
