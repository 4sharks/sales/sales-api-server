<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListPriceItemModel extends Model
{
    use HasFactory;
    protected $table = 'list_price_items';
    protected $guarded = [];
}
