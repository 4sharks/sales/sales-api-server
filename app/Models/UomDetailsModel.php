<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UomDetailsModel extends Model
{
    use HasFactory;
    protected $table = 'uom_details';

    protected $guarded = [];
}
