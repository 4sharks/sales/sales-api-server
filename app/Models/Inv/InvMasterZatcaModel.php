<?php

namespace App\Models\Inv;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvMasterZatcaModel extends Model
{
    use HasFactory;

    protected $table = 'inv_master_zatca';
    protected $guarded = [];

}
