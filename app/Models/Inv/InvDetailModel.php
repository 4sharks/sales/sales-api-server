<?php

namespace App\Models\Inv;

use App\Models\UomDetailsModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvDetailModel extends Model
{
    use HasFactory;


    protected $table = 'inv_details';

    protected $guarded = [];

    public function uomRelation(){
        return $this->hasOne(UomDetailsModel::class,'id','uom');
    }

}
