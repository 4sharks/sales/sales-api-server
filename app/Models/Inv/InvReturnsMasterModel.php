<?php

namespace App\Models\Inv;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvReturnsMasterModel extends Model
{
    use HasFactory;
    protected $table = 'inv_returns_master';
    protected $guarded = [];

    public function invDetails(){
        return $this->hasMany(InvReturnsDetailModel::class,'inv_returns_master_id' ,'id');
    }
}
