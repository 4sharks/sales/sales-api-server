<?php

namespace App\Models\Inv;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvZatcaResponse extends Model
{
    use HasFactory;

    protected $table = 'inv_zatca_response';
    protected $guarded = [];


}
