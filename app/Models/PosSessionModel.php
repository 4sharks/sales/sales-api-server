<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PosSessionModel extends Model
{
    use HasFactory;

    protected $table = 'pos_sessions';

    protected $guarded = [];

}
