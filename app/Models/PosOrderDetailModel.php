<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PosOrderDetailModel extends Model
{
    use HasFactory;
    protected $table = 'pos_order_detail';

    protected $guarded = [];
}
