<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UomMasterModel extends Model
{
    use HasFactory;
    protected $table = 'uom_master';

    protected $guarded = [];

    public function uomDetails(){
        return $this->hasMany(UomDetailsModel::class,'id_uom_master' ,'id');
    }
}
