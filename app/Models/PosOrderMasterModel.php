<?php

namespace App\Models;

use App\Models\Inv\PaymentTransaction;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PosOrderMasterModel extends Model
{
    use HasFactory;

    protected $table = 'pos_order_master';

    protected $guarded = [];

    public function items(){
        return $this->hasMany(PosOrderDetailModel::class,'order_id' ,'id');
    }

    public function payment_transactions(){
        return $this->hasMany(PaymentTransaction::class,'inv_id' ,'id');
    }

}
