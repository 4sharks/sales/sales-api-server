<?php

namespace App\Models\Quote;

use App\Models\UomDetailsModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuoteDetailModel extends Model
{
    use HasFactory;

    protected $table = 'quotes_details';
    protected $guarded = [];

    public function uomRelation(){
        return $this->hasOne(UomDetailsModel::class,'id','uom');
    }
}
