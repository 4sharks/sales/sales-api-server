<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UomTemplateModel extends Model
{
    use HasFactory;
    protected $table = 'uom_templates';

    protected $guarded = [];

    public function uomMaster(){
        return $this->hasOne(UomMasterModel::class,'id_uom_template' ,'id');
    }
}
