<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListPriceModel extends Model
{
    use HasFactory;

    protected $table = 'list_price';
    protected $guarded = [];

    public function items(){
        return $this->hasMany(ListPriceItemModel::class,'list_price_id' ,'id');
    }

}
