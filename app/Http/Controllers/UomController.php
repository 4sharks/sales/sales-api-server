<?php

namespace App\Http\Controllers;

use App\Models\Inv\InvDetailModel;
use App\Models\UomDetailsModel;
use App\Models\UomMasterModel;
use App\Models\UomTemplateModel;
use Illuminate\Http\Request;

class UomController extends Controller
{
    public function get_by_id($id){
        $result = UomTemplateModel::with('uomMaster','uomMaster.uomDetails')->find($id);
        if($result){
            return response()->json([
                "status" => 1 ,
                "message" => "success",
                "data" => $result
            ],200);
        }
        return response()->json([
            "status" => 0 ,
            "message" => "error",
            "data" => null
        ],401);
    }
    public function get_by_tenant($tenant_id){
        $result = UomTemplateModel::with('uomMaster','uomMaster.uomDetails')->where([
            "tenant_id" => $tenant_id
        ])->get();
        if($result){
            return response()->json([
                "status" => 1 ,
                "message" => "success",
                "data" => $result
            ],200);
        }
        return response()->json([
            "status" => 0 ,
            "message" => "error",
            "data" => null
        ],401);
    }

    public function store(Request $request)
    {

        $template = UomTemplateModel::create([
            "name" => $request->input('template_name'),
            "is_active" => $request->input('template_is_active'),
            "tenant_id" => $request->input('tenant_id'),
        ]);
        if($template){
            $result['template'] = $template;
            $uom_master = UomMasterModel::create([
                "id_uom_template" => $template->id,
                "name" => $request->input('uom_master_name'),
                "short_name" => $request->input('uom_master_short_name'),
                "tenant_id" => $request->input('tenant_id'),
            ]);

            if($uom_master){
                $result['template']['master'] = $uom_master;
                if(count($request->input('uom_details')) > 0){
                    // $uom_details = array();
                    $i=0;
                    foreach($request->input('uom_details') as $detail){
                        $uom_details[$i] = UomDetailsModel::create([
                            "id_uom_master" => $uom_master->id,
                            "name" => $detail['uom_details_name'],
                            "short_name" => $detail['uom_details_short_name'],
                            "rate" => $detail['uom_details_rate'],
                            "tenant_id" => $request->input('tenant_id'),
                        ]);
                        $i++;
                    }
                    if($uom_details){
                        $result['template']['master']['details'] = $uom_details;
                        return response()->json([
                            "status" => 1 ,
                            "message" => "success",
                            "data" => $result
                        ],201);
                    }

                }
            }
            return response()->json([
                "status" => 0 ,
                "message" => "error",
                "data" => null
            ],401);
        }

    }
    public function update(Request $request)
    {

        $template = UomTemplateModel::where([
            'id' => $request->input('template_id'),
            'tenant_id' => $request->input('tenant_id'),
        ])->update([
            "name" => $request->input('template_name'),
            "is_active" => $request->input('template_is_active'),
        ]);
        if($template){
            $result['template'] = $template;
            $uom_master = UomMasterModel::where([
                'id' => $request->input('uom_master_id'),
                'tenant_id' => $request->input('tenant_id'),
            ])->update([
                "name" => $request->input('uom_master_name'),
                "short_name" => $request->input('uom_master_short_name'),
            ]);

            if($uom_master){
                // $result['template']['master'] = $uom_master;
                if(count($request->input('uom_details')) > 0){
                    // $uom_details = array();
                    $i=0;
                    foreach($request->input('uom_details') as $detail){
                        $uom_details[$i] = UomDetailsModel::where([
                            'id' => $detail['uom_details_id'],
                            'tenant_id' => $request->input('tenant_id'),
                            ])->update([
                            "name" => $detail['uom_details_name'],
                            "short_name" => $detail['uom_details_short_name'],
                            "rate" => $detail['uom_details_rate'],
                        ]);
                        $i++;
                    }
                    //if($uom_details){
                        // $result['template']['master']['details'] = $uom_details;
                        return response()->json([
                            "status" => 1 ,
                            "message" => "success update",
                            "data" => null
                        ],201);
                    //}

                }
            }
            return response()->json([
                "status" => 0 ,
                "message" => "error update",
                "data" => null
            ],401);
        }

    }

    public function delete($id){
        $check = InvDetailModel::where([
            "uom_template_id" => $id
        ])->count();
        if($check > 0){
            return response()->json([
                "status" => 0 ,
                "message" => "error deleting this UOM template is using",
                "data" => null
            ],401);
        }
        $master = UomMasterModel::where([
            "id_uom_template" => $id
            ])->get()->first();
        $delete_details = UomDetailsModel::where([
            "id_uom_master" => $master->id
            ])->delete();
        $delete_master = UomMasterModel::where([
            "id_uom_template" => $id
        ])->delete();
        $delete_template = UomTemplateModel::find($id)->delete();
        return response()->json([
            "status" => 1 ,
            "message" => "success deleting",
            "data" => null
        ],201);

    }
    public function delete_uom_detail($id){
        $check = InvDetailModel::where([
            "uom" => $id
        ])->count();
        if($check > 0){
            return response()->json([
                "status" => 0 ,
                "message" => "error deleting this UOM  is using",
                "data" => null
            ],401);
        }
        $delete_template = UomDetailsModel::find($id)->delete();
        return response()->json([
            "status" => 1 ,
            "message" => "success deleting",
            "data" => null
        ],201);

    }
}
