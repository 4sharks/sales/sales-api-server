<?php

namespace App\Http\Controllers\Quote;

use App\Http\Controllers\Controller;
use App\Services\QuoteService;
use Illuminate\Http\Request;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(QuoteService $quoteService,$tenant_id,$company_id,$branch_id)
    {
        return $quoteService->getAllInvoices($tenant_id,$company_id,$branch_id);
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request , QuoteService $quoteService)
    {
        return $quoteService->createInv($request);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id,QuoteService $quoteService)
    {
        return $quoteService->showInv($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id , QuoteService $quoteService)
    {
        return $quoteService->updateInv($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     */

     public function destroy(string $id, QuoteService $quoteService)
    {
        return $quoteService->deleteInv($id);
    }

    public function get_last_invno_by_tenant(Request $request, QuoteService $quoteService){
        $tenant_id = $request->input('tenant_id');
        $company_id = $request->input('company_id');
        $branch_id = $request->input('branch_id');

        return $quoteService->getLastInvNoByTenant($tenant_id,$company_id,$branch_id);
    }

    public function get_last_invno_by_tenant_with_prefix(Request $request, QuoteService $quoteService){
        $tenant_id = $request->input('tenant_id');
        $company_id = $request->input('company_id');
        $branch_id = $request->input('branch_id');
        $prefix = $request->input('prefix');

        return $quoteService->getLastInvNoByTenant($tenant_id,$company_id,$branch_id,'with_prefix',$prefix);
    }

    public function sumInvoicesByMonth(QuoteService $quoteService,$tenant_id,$company_id,$branch_id,$month)
    {
        return $quoteService->getSumInvoicesByMonth($tenant_id,$company_id,$branch_id,$month);
    }

    public function sumInvoicesByYear(QuoteService $quoteService,$tenant_id,$company_id,$branch_id,$year)
    {
        return $quoteService->getSumInvoicesByYear($tenant_id,$company_id,$branch_id,$year);
    }

    public function sumInvoicesPaidByYear(QuoteService $quoteService,$tenant_id,$company_id,$branch_id,$year,$is_paid)
    {
        return $quoteService->getSumInvoicesPaidByYear($tenant_id,$company_id,$branch_id,$year,$is_paid);
    }

    public function sumGroupByMonth(QuoteService $quoteService,$tenant_id,$company_id,$branch_id,$year)
    {
        return $quoteService->getSumGroupByMonth($tenant_id,$company_id,$branch_id,$year);
    }

    public function sumBySalesman(QuoteService $quoteService,$tenant_id,$company_id,$branch_id,$year)
    {
        return $quoteService->getSumBySalesman($tenant_id,$company_id,$branch_id,$year);
    }

    public function getByInvNo(QuoteService $quoteService,$tenant_id,$company_id,$branch_id,$inv_no)
    {
        return $quoteService->getByInvNo($tenant_id,$company_id,$branch_id,$inv_no);
    }

    public function getByCustomer(QuoteService $quoteService,$tenant_id,$company_id,$branch_id,$customer_id)
    {
        return $quoteService->getByCustomer($tenant_id,$company_id,$branch_id,$customer_id);
    }
}
