<?php

namespace App\Http\Controllers;

use App\Models\ListPriceItemModel;
use Illuminate\Http\Request;

class ListPriceItemsController extends Controller
{
    public function index($tenant_id,$company_id,$branch_id){
        $result = ListPriceItemModel::where([
            "tenant_id" => $tenant_id,
            "company_id" => $company_id,
            "branch_id" => $branch_id,
        ])->get();
        if($result){
            return response()->json([
                "status" => 1 ,
                "message" => "success ",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "message" => "error fetching results",
            "data" => null
        ],401);
    }

    public function get_list_items($list_price_id){
        $result = ListPriceItemModel::where([
            "list_price_id" => $list_price_id
        ])->get();
        if($result){
            return response()->json([
                "status" => 1 ,
                "message" => "success ",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "message" => "error during fetch operation",
            "data" => null
        ],401);
    }

    public function store(Request $request){
        $result = [];
        $i=0;
        foreach($request->all() as $item){

            $tenant_id = $item['tenant_id'];
            $company_id = $item['company_id'];
            $branch_id = $item['branch_id'];
            $product_id = $item['product_id'];
            $product_name = $item['product_name'];
            $product_sale_price = $item['product_sale_price'];
            $list_price_id = $item['list_price_id'];
            if(!$tenant_id && !$company_id && !$branch_id && !$product_name && !$list_price_id){
                return response()->json([
                    "status" => 0 ,
                    "message" => "All fields are required",
                    "data" => null
                ],401);
            }
            $result[$i] = ListPriceItemModel::create([
                "tenant_id" => $tenant_id,
                "company_id" => $company_id,
                "branch_id" => $branch_id,
                "list_price_id" => $list_price_id,
                "product_id" => $product_id,
                "product_name" => $product_name,
                "product_sale_price" => $product_sale_price,

            ]);
            $i++;
        }
        if(count($result) > 0){
            return response()->json([
                "status" => 1 ,
                "message" => "success ",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "message" => "error fetching results",
            "data" => null
        ],401);
    }
    public function update(Request $request,$id){
        $product_id = $request->input('product_id');
        $product_name = $request->input('product_name');
        $product_sale_price = $request->input('product_sale_price');
        $list_price_id = $request->input('list_price_id');
        if(!$product_id && !$id && !$list_price_id){
            return response()->json([
                "status" => 0 ,
                "message" => "All fields are required",
                "data" => null
            ],401);
        }
        $result = ListPriceItemModel::find($id)->update([
            "list_price_id" => $list_price_id,
            "product_id" => $product_id,
            "product_name" => $product_name,
            "product_sale_price" => $product_sale_price,
        ]);
        if($result){
            return response()->json([
                "status" => 1 ,
                "message" => "success ",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "message" => "error during update operation",
            "data" => null
        ],401);
    }

    public function delete($id){
        $result = ListPriceItemModel::find($id)->delete();
        if($result){
            return response()->json([
                "status" => 1 ,
                "message" => "success ",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "message" => "error during delete operation",
            "data" => null
        ],401);
    }

    public function delete_all(Request $request){
        $ids = $request->input('ids');
        $result = ListPriceItemModel::whereIn('id',$ids)->delete();
        if($result){
            return response()->json([
                "status" => 1 ,
                "message" => "success ",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "message" => "error during delete operation",
            "data" => null
        ],401);
    }

}
