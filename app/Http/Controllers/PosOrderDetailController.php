<?php

namespace App\Http\Controllers;

use App\Models\PosOrderDetailModel;
use Illuminate\Http\Request;

class PosOrderDetailController extends Controller
{
    public function get_By_order_id($order_id){
        $result = PosOrderDetailModel::where([
            "order_id" => $order_id,
        ])->get();
        if($result){
            return response()->json([
                "status" => 1 ,
                "result" => "success",
                "data" => $result
            ],201);
        }

        return response()->json([
            "status" => 0 ,
            "result" => "error",
            "data" => $result
        ],401);
    }

    public function add_item(Request $request){

        $result = PosOrderDetailModel::create([
            "order_id" => $request->input('order_id'),
            "order_no" => $request->input('order_no'),
            "product_id" => $request->input('product_id'),
            "product_name" => $request->input('product_name'),
            "product_desc" => $request->input('product_desc'),
            "unit_id" => $request->input('unit_id'),
            "qty" => $request->input('qty'),
            "price" => $request->input('price'),
            "total_price" => $request->input('total_price'),
            "product_discount" => $request->input('product_discount'),
            "product_net_total" => $request->input('product_net_total'),
            "product_vat" => $request->input('product_vat'),
            "product_net_total_with_vat" => $request->input('product_net_total_with_vat'),
        ]);

        if($result){
            return response()->json([
                "status" => 1 ,
                "result" => "success",
                "data" => $result
            ],201);
        }

        return response()->json([
            "status" => 0 ,
            "result" => "error",
            "data" => $result
        ],401);
    }
    public function update_item(Request $request,$id){

        $result = PosOrderDetailModel::find($id)->update([
            "product_id" => $request->input('product_id'),
            "product_name" => $request->input('product_name'),
            "product_desc" => $request->input('product_desc'),
            "unit_id" => $request->input('unit_id'),
            "qty" => $request->input('qty'),
            "price" => $request->input('price'),
            "total_price" => $request->input('total_price'),
            "product_discount" => $request->input('product_discount'),
            "product_net_total" => $request->input('product_net_total'),
            "product_vat" => $request->input('product_vat'),
            "product_net_total_with_vat" => $request->input('product_net_total_with_vat'),
        ]);

        if($result){
            return response()->json([
                "status" => 1 ,
                "result" => "success",
                "data" => $result
            ],201);
        }

        return response()->json([
            "status" => 0 ,
            "result" => "error",
            "data" => $result
        ],401);
    }
    public function update_qty($id,$qty){

        $result = PosOrderDetailModel::find($id)->update([
            "qty" => $qty,
        ]);

        if($result){
            return response()->json([
                "status" => 1 ,
                "result" => "success",
                "data" => $result
            ],201);
        }

        return response()->json([
            "status" => 0 ,
            "result" => "error",
            "data" => $result
        ],401);
    }

    public function delete($id){
        $result = PosOrderDetailModel::find($id)->delete();
        if($result){
            return response()->json([
                "status" => 1 ,
                "result" => "success",
                "data" => $result
            ],201);
        }

        return response()->json([
            "status" => 0 ,
            "result" => "error",
            "data" => $result
        ],401);
    }


}
