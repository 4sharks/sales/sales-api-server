<?php

namespace App\Http\Controllers\Inv;

use App\Http\Controllers\Controller;
use App\Models\Inv\InvMasterZatcaModel;
use App\Models\Inv\InvZatcaResponse;
use Illuminate\Http\Request;

class ZatcaController extends Controller
{
    public function store(Request $request){
        if($request->input("inv_id") && $request->input("inv_no") ){
            $old_inv = InvMasterZatcaModel::where([
                "inv_id" => $request->input("inv_id"),
                "inv_no" => $request->input("inv_no"),
            ])->orderBy("id","desc")->first();

            if($old_inv){
                InvMasterZatcaModel::where([
                    "inv_id" => $request->input("inv_id"),
                    "inv_no" => $request->input("inv_no"),
                ])->update([
                    "zatca_status_final" => trim($old_inv->zatca_status_final)."_ARCHIVE"
                ]);
            }

            $result = InvMasterZatcaModel::create($request->except('hash','xml','inv_status','inv_type','qr'));
            InvZatcaResponse::create([
                "inv_id" => $request->input("inv_id"),
                "inv_no" => $request->input("inv_no"),
                "qr" => $request->input("zatca_qr"),
                "hash" => $request->input("hash"),
                "xml" => $request->input("xml"),
                "inv_type" => $request->input("zatca_inv_type"),
                "inv_status" => $request->input("zatca_status_final"),
                "tenant_id" => $request->input("tenant_id"),
            ]);
            if($result){
                return response()->json([
                    "status" => 1,
                    "message" =>"Successfully",
                    "data" => $result
                ], 200);
            }
        }
        return response()->json([
            "status" => 0,
            "message" =>"Error",
            "data" => $result
        ], 404);
    }

    public function get_last_zatca($tenant_id){
        $result =  InvZatcaResponse::where([
            "tenant_id" => $tenant_id
        ])->orderBy('id','desc')->get()->first();

        if(!$result){
            return [
                "hash" => "NWZlY2ViNjZmZmM4NmYzOGQ5NTI3ODZjNmQ2OTZjNzljMmRiYzIzOWRkNGU5MWI0NjcyOWQ3M2EyN2ZiNTdlOQ"
            ];
        }
        return $result;
    }
}
