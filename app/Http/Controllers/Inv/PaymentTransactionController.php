<?php

namespace App\Http\Controllers\Inv;

use App\Http\Controllers\Controller;
use App\Models\Inv\InvMasterModel;
use App\Models\Inv\InvReturnsMasterModel;
use App\Models\Inv\PaymentTransaction;
use App\Models\PosOrderDetailModel;
use App\Models\PosOrderMasterModel;
use Illuminate\Http\Request;

class PaymentTransactionController extends Controller
{
    public function index($tenant_id,$company_id,$branch_id){
        return PaymentTransaction::where([
            'tenant_id' =>  $tenant_id,
            'company_id' => $company_id,
            'branch_id' =>  $branch_id
        ])->orderBy('id','desc')->get();
    }

    public function filterBy(Request $request){

        $tenant_id = $request->input('tenant_id');
        $company_id = $request->input('company_id');
        $branch_id = $request->input('branch_id');
        $payment_method_id = $request->input('payment_method_id');
        $page_limit = $request->input('page_limit');
        $created_by = $request->input('created_by');
        $type = $request->input('type');
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');

        if(!$tenant_id || !$company_id || !$branch_id){
            return response()->json([
                "status" => 0,
                "message" => 'Invalid fetch',
                "data" => [],
            ]);
        }

        if(!$page_limit){
            $page_limit = 25;
        }

        $cond = [
            'tenant_id' =>  $tenant_id,
            'company_id' => $company_id,
            'branch_id' =>  $branch_id
        ];

        if($payment_method_id){
            $cond['payment_method_id'] = $payment_method_id;
        }

        if($created_by){
            $cond['created_by'] = $created_by;
        }

        if($type){
            if(trim($type) !='all'){
                $cond['type'] = $type;
            }
        }

        if($start_date){
            $start_date = date('Y-m-d',strtotime($start_date));
        }else{
            $start_date = date('Y-m-d',strtotime(date('Y-01-01')));
        }

        if($end_date){
            $end_date = date('Y-m-d',strtotime($end_date));
        }else{
            $end_date = date('Y-m-d',strtotime(date('Y-12-31')));
        }


        return PaymentTransaction::with(['payment_method:id,is_active','payment_method.description:payment_method_id,name,lang'])->where($cond)->orderBy('id','desc')->whereBetween('created_at',[$start_date,$end_date])->paginate($page_limit);
    }

    public function getTrannsactionsByInvId($inv_id){
        return PaymentTransaction::with('payment_method.description')->where([
            'inv_id' => $inv_id
        ])->get();
    }

    public function store(Request $request){
        foreach($request->all() as $key){
            $type = $key['type'];
            $inv_id = $key['inv_id'];
            $inv_no = $key['inv_no'];
            //$transaction_no = $key['transaction_no'];
            $transaction_no = uniqid($type.'_');
            $amount = $key['amount'];
            $payment_method_id = $key['payment_method_id'];
            $notes = $key['notes'];
            $tenant_id = $key['tenant_id'];
            $company_id = $key['company_id'];
            $branch_id = $key['branch_id'];
            $created_by = $key['created_by'];

            if(!$tenant_id || !$company_id || !$branch_id || !$inv_id || !$payment_method_id || !$amount ) return 'all fields are required';

            $added[] = PaymentTransaction::create([
                'type' => trim($type) ,
                'inv_id' => $inv_id,
                'inv_no' => $inv_no,
                'transaction_no' => $transaction_no,
                'amount' => $amount,
                'payment_method_id' => $payment_method_id,
                'notes' => $notes,
                'tenant_id' => $tenant_id,
                'company_id' => $company_id,
                'branch_id' => $branch_id,
                'created_by' => $created_by
            ]);
        }
        if($added){
            $sumTransaction = PaymentTransaction::where([
                'inv_id' => $inv_id,
                'inv_no' => $inv_no,
                'tenant_id' => $tenant_id,
                'company_id' => $company_id,
                'branch_id' => $branch_id,
            ])->sum('amount');
            if( $type === 'out' ){
                $inv = InvReturnsMasterModel::where([
                    'inv_id' => $inv_id,
                    'is_paid' => false,
                ])->first();
                $invTotal = $inv->net_amount;
                if($sumTransaction == $invTotal ){
                    $inv->is_paid = true;
                    $inv->save();
                }
            }else{
                $inv = InvMasterModel::find($inv_id);
                $invTotal = $inv->net_amount;
                if($sumTransaction == $invTotal ){
                    $inv->is_paid = true;
                    $inv->save();
                }
            }
            return response()->json(['status' => 1,'data' => ['transactions' => $added , "invoiceTotal" => $invTotal , "sumTransaction" => $sumTransaction ]  ]);
        }

    }
    public function store_multi(Request $request){
        $result = [];
        foreach($request->input('items') as $item){
            $result[] = PaymentTransaction::create([
                'type' => 'POS',
                'inv_id' => $request->input('inv_id'),
                'inv_no' => $request->input('inv_no'),
                'amount' => $item['amount'],
                'payment_method_id' => $item['payment_method_id'],
                'notes' => $item['notes'],
                'tenant_id' => $request->input('tenant_id'),
                'company_id' => $request->input('company_id'),
                'branch_id' => $request->input('branch_id'),
                'created_by' => $request->input('inv_id')
            ]);
        }

        if(count($result) > 0){
            $inv = PosOrderMasterModel::find($request->input('inv_id'));
            if($inv){
                $sumOrderDetails = PosOrderDetailModel::where([
                    'order_id' => $request->input('inv_id')
                ])->sum('total_price');
                $total = intval($sumOrderDetails);
                $vat = (intval($sumOrderDetails) * 15)/115;
                $inv->vat_amount = $vat;
                $inv->net_amount = $total;
                $inv->total_amount = $total - $vat;

                if($sumOrderDetails == 0){
                    $inv->is_paid = true;

                }else{
                    $sumTransaction = PaymentTransaction::where([
                        'inv_id' => $request->input('inv_id'),
                        'tenant_id' => $request->input('tenant_id'),
                        'company_id' => $request->input('company_id'),
                        'branch_id' => $request->input('branch_id'),
                    ])->sum('amount');
                        if($sumTransaction){
                                if($sumTransaction ==  $sumOrderDetails ){
                                    $inv->is_paid = true;

                                }
                        }
                }
                $inv->save();
               // $result['inv'] = $inv;
            }
        }

        return response()->json(['status' => 1,'data' => $result ]);



    }

}
