<?php

namespace App\Http\Controllers\Inv;

use App\Http\Controllers\Controller;
use App\Services\InvReturnsService;
use Illuminate\Http\Request;

class InvReturnsController extends Controller
{
    private $invService;

    public function __construct()
    {
        $this->invService = new InvReturnsService();
    }

    public function index($tenant_id,$company_id,$branch_id)
    {
        return $this->invService->getAllInvoices($tenant_id,$company_id,$branch_id);
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request )
    {
        return $this->invService->createInv($request);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        return $this->invService->showInv($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id )
    {
        return $this->invService->updateInv($request,$id);
    }

    public function setPaid(Request $request, string $id )
    {
        $result = $this->invService->setPaid($request,$id);
        if($result){
            return response()->json([
                "status" => 1 ,
                "result" => "success",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "result" => "error",
            "data" => $result
        ],401);
    }

    /**
     * Remove the specified resource from storage.
     */

    public function destroy(string $id)
    {
        return $this->invService->deleteInv($id);
    }

    public function get_last_invno_by_tenant(Request $request){
        $tenant_id = $request->input('tenant_id');
        $company_id = $request->input('company_id');
        $branch_id = $request->input('branch_id');

        return $this->invService->getLastInvNoByTenant($tenant_id,$company_id,$branch_id);
    }

    public function get_last_invno_by_tenant_with_prefix(Request $request){
        $tenant_id = $request->input('tenant_id');
        $company_id = $request->input('company_id');
        $branch_id = $request->input('branch_id');
        $prefix = $request->input('prefix');

        return $this->invService->getLastInvNoByTenant($tenant_id,$company_id,$branch_id,'with_prefix',$prefix);
    }

    public function sumInvoicesByMonth($tenant_id,$company_id,$branch_id,$month)
    {
        return $this->invService->getSumInvoicesByMonth($tenant_id,$company_id,$branch_id,$month);
    }

    public function sumInvoicesByYear($tenant_id,$company_id,$branch_id,$year)
    {
        return $this->invService->getSumInvoicesByYear($tenant_id,$company_id,$branch_id,$year);
    }

    public function sumInvoicesPaidByYear($tenant_id,$company_id,$branch_id,$year,$is_paid)
    {
        return $this->invService->getSumInvoicesPaidByYear($tenant_id,$company_id,$branch_id,$year,$is_paid);
    }

    public function sumGroupByMonth($tenant_id,$company_id,$branch_id,$year)
    {
        return $this->invService->getSumGroupByMonth($tenant_id,$company_id,$branch_id,$year);
    }

    public function sumBySalesman($tenant_id,$company_id,$branch_id,$year)
    {
        return $this->invService->getSumBySalesman($tenant_id,$company_id,$branch_id,$year);
    }

    public function topProducts($tenant_id,$company_id,$branch_id,$year)
    {
        return $this->invService->getTopProducts($tenant_id,$company_id,$branch_id,$year);
    }

    public function getByInvNo($tenant_id,$company_id,$branch_id,$inv_no)
    {
        return $this->invService->getByInvNo($tenant_id,$company_id,$branch_id,$inv_no);
    }

    public function getByCustomer($tenant_id,$company_id,$branch_id,$customer_id)
    {
        return $this->invService->getByCustomer($tenant_id,$company_id,$branch_id,$customer_id);
    }

    public function getFilterBy(Request $request){
        $tenant_id = $request->input('tenant_id');
        $company_id = $request->input('company_id');
        $branch_id = $request->input('branch_id');
        $customer_id = $request->input('customer_id');
        $salesman_id = $request->input('salesman_id');
        $inv_start_date = date('Y-m-d',strtotime($request->input('inv_start_date')));
        $inv_end_date = date('Y-m-d',strtotime($request->input('inv_end_date')));

        $fetch = $this->invService->filterBy($tenant_id,$company_id,$branch_id,$customer_id,$salesman_id,$inv_start_date,$inv_end_date);

        if($fetch){
            return $fetch;
        }else{
            return response()->json([
                "status" => 0,
                "message" => 'Invalid fetch',
                "data" => [],
            ]);
        }
    }

}
