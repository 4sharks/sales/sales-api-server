<?php

namespace App\Http\Controllers;

use App\Models\ListPriceModel;
use Illuminate\Http\Request;

class ListPriceController extends Controller
{
    public function index($tenant_id,$company_id,$branch_id){
        $result = ListPriceModel::with('items')->where([
            "tenant_id" => $tenant_id,
            "company_id" => $company_id,
            "branch_id" => $branch_id,
        ])->get();
        if($result){
            return response()->json([
                "status" => 1 ,
                "message" => "success ",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "message" => "error fetching results",
            "data" => null
        ],401);
    }

    public function get_list($id){
        $result = ListPriceModel::with('items')->find($id);
        if($result){
            return response()->json([
                "status" => 1 ,
                "message" => "success ",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "message" => "error during fetch operation",
            "data" => null
        ],401);
    }

    public function store(Request $request){
        $tenant_id = $request->input('tenant_id');
        $company_id = $request->input('company_id');
        $branch_id = $request->input('branch_id');
        $name = $request->input('name');
        $is_active = $request->input('is_active');
        if(!$tenant_id && !$company_id && !$branch_id && !$name){
            return response()->json([
                "status" => 0 ,
                "message" => "All fields are required",
                "data" => null
            ],401);
        }
        $result = ListPriceModel::create([
            "tenant_id" => $tenant_id,
            "company_id" => $company_id,
            "branch_id" => $branch_id,
            "name" => $name,
            "is_active" => $is_active,
        ]);
        if($result){
            return response()->json([
                "status" => 1 ,
                "message" => "success ",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "message" => "error fetching results",
            "data" => null
        ],401);
    }
    public function update(Request $request,$id){
        $name = $request->input('name');
        $is_active = $request->input('is_active');
        if(!$name && !$id){
            return response()->json([
                "status" => 0 ,
                "message" => "All fields are required",
                "data" => null
            ],401);
        }
        $result = ListPriceModel::find($id)->update([
            "name" => $name,
            "is_active" => $is_active,
        ]);
        if($result){
            return response()->json([
                "status" => 1 ,
                "message" => "success ",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "message" => "error during update operation",
            "data" => null
        ],401);
    }

    public function delete($id){
        $result = ListPriceModel::find($id)->delete();
        if($result){
            return response()->json([
                "status" => 1 ,
                "message" => "success ",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "message" => "error during delete operation",
            "data" => null
        ],401);
    }

}
