<?php

namespace App\Http\Controllers;

use App\Models\PosOrderMasterModel;
use Illuminate\Http\Request;

class PosOrderMasterController extends Controller
{
    public function create($tenant_id,$company_id,$branch_id,$created_by)
    {
        $order_date = date('Y-m-d H:i:s');
        $order = PosOrderMasterModel::where([
            "tenant_id"  => $tenant_id,
            "company_id" => $company_id,
            "branch_id"  => $branch_id,
        ])->orderBy('id','desc')->first();
        if($order){
            $oder_no  = intval($order->order_no) + 1;
        }else{
            $oder_no  = 1   ;
        }

        $result = PosOrderMasterModel::create([
            "order_no" => $oder_no  ,
            "order_date" => $order_date,
            "tenant_id"  => $tenant_id,
            "company_id" => $company_id,
            "branch_id"  => $branch_id,
            "created_by" => $created_by
        ]);

        if($result){
            return response()->json([
                "status" => 1 ,
                "result" => "success",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "result" => "error",
            "data" => $result
        ],401);

    }

    public function get_order($order_id)
    {
        $result = PosOrderMasterModel::with('items','payment_transactions','payment_transactions.payment_method.description')->find($order_id);
        if($result){
            return response()->json([
                "status" => 1 ,
                "result" => "success",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "result" => "error",
            "data" => $result
        ],401);
    }
    public function get_open_orders_by_user($tenant_id,$company_id,$branch_id,$created_by)
    {
        $result = PosOrderMasterModel::with('items','payment_transactions','payment_transactions.payment_method.description')->where([
            "status_id"  => null,
            "tenant_id"  => $tenant_id,
            "company_id" => $company_id,
            "branch_id"  => $branch_id,
            "created_by" => $created_by
        ])->get();
        if($result){
            return response()->json([
                "status" => 1 ,
                "result" => "success",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "result" => "error",
            "data" => $result
        ],401);
    }


}
