<?php

namespace App\Http\Controllers;

use App\Models\PosSessionModel;
use Illuminate\Http\Request;

class PosSessionController extends Controller
{
    public function gen_session_code($tenant_id,$company_id,$branch_id){
        $last = PosSessionModel::where([
            "tenant_id"  => $tenant_id,
            "company_id" => $company_id,
            "branch_id"  => $branch_id,
        ])->orderBy('id','desc')->first();
        if($last){
            if($last['session_code']){
                $sp = explode('-',@$last['session_code']);
                if(count($sp) > 0){
                    $code = intval($sp[1]) + 1;
                    $result = "POS-".str_pad($code,6,"0",STR_PAD_LEFT);
                }
            }else{
                $result = "POS-".str_pad('1',6,"0",STR_PAD_LEFT);
            }
        }else{
            $result = "POS-".str_pad('1',6,"0",STR_PAD_LEFT);
        }

        if($result){
            return response()->json([
                "status" => 1 ,
                "result" => "success",
                "data" => [
                    "session_code" => $result
                ]
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "result" => "error",
            "data" => $result
        ],401);

    }

    public function create_session(Request $request){
        $tenant_id = $request->input('tenant_id');
        $company_id = $request->input('company_id');
        $branch_id = $request->input('branch_id');
        $created_by = $request->input('created_by');
        $session_code = $request->input('session_code');
        $open_cash = $request->input('open_cash');
        $open_notes = $request->input('open_notes');
        $result = PosSessionModel::create([
            "session_code"  => $session_code,
            "open_cash"  => $open_cash,
            "open_notes"  => $open_notes,
            "tenant_id"  => $tenant_id,
            "company_id" => $company_id,
            "branch_id"  => $branch_id,
            "created_by" => $created_by
        ]);
        if($result){
            return response()->json([
                "status" => 1 ,
                "result" => "success",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "result" => "error",
            "data" => $result
        ],401);
    }
    public function close_session(Request $request,$session_id){
        $close_cash = $request->input('close_cash');
        $colse_notes = $request->input('colse_notes');
        $result = PosSessionModel::find($session_id)
        ->update([
            "close_cash"  => $close_cash,
            "colse_notes"  => $colse_notes,
            "status" => 5
        ]);
        if($result){
            return response()->json([
                "status" => 1 ,
                "result" => "success",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "result" => "error",
            "data" => $result
        ],401);
    }
}
