<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('inv_master_zatca', function (Blueprint $table) {
            $table->id();
            $table->integer('inv_id');
            $table->string('branch_code');
            $table->string('inv_no');
            $table->enum('zatca_inv_type',['simplified','simplified_debit_note','simplified_credit_note','standard','standard_debit_note','standard_credit_note'])->nullable();
            $table->enum('zatca_status',['CLEARED','REPORTED'])->nullable();
            $table->string('zatca_status_final')->nullable();
            $table->string('zatca_qr')->nullable();
            $table->string('clearedInvoice')->nullable();
            $table->text('zatca_response')->nullable();
            $table->string('notes')->nullable();
            $table->string('cat_tax')->nullable();
            $table->string('class_tax')->nullable();
            $table->string('currency_code')->nullable();
            $table->string('buyer_name')->nullable();
            $table->string('buyer_vat_no')->nullable();
            $table->string('buyer_street')->nullable();
            $table->string('buyer_building')->nullable();
            $table->string('buyer_city')->nullable();
            $table->string('buyer_country_code')->nullable();
            $table->string('buyer_postal')->nullable();
            $table->string('tenant_id')->nullable();
            $table->string('company_id')->nullable();
            $table->string('branch_id')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('inv_master_zatca');
    }
};
