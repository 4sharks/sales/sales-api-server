<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('inv_zatca_response', function (Blueprint $table) {
            $table->id();
            $table->string('inv_no')->nullable();
            $table->integer('inv_id')->nullable();
            $table->string('qr')->nullable();
            $table->string('hash')->nullable();
            $table->string('xml')->nullable();
            $table->string('inv_type')->nullable();
            $table->string('inv_status')->nullable();
            $table->string('tenant_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('inv_zatca_response');
    }
};
