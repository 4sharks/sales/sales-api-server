<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('list_price_items', function (Blueprint $table) {
            $table->id();
            $table->integer('list_price_id');
            $table->string('product_id');
            $table->string('product_name');
            $table->string('product_sale_price');
            $table->string('branch_id');
            $table->string('company_id');
            $table->string('tenant_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('list_price_items');
    }
};
