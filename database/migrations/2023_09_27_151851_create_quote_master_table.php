<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('quotes_master', function (Blueprint $table) {
            $table->string('inv_no');
            $table->string('inv_ref')->nullable();
            $table->timestamp('inv_date');
            $table->date('inv_date_due');
            $table->float('total_amount');
            $table->float('discount_amount')->nullable();
            $table->string('promo_code_id')->nullable();
            $table->string('promo_code')->nullable();
            $table->float('vat_amount')->nullable();
            $table->float('net_amount')->nullable();
            $table->tinyInteger('type_id')->nullable();
            $table->tinyInteger('status_id')->nullable();
            $table->boolean('is_paid')->default(false);
            $table->string('inv_tags')->nullable();
            $table->string('customer_id')->nullable();
            $table->string('customer_name')->nullable();
            $table->string('salesman_id')->nullable();
            $table->string('salesman_name')->nullable();
            $table->string('title')->nullable();
            $table->string('title_ar')->nullable();
            $table->text('notes')->nullable();
            $table->text('terms')->nullable();
            $table->string('tenant_id')->nullable();
            $table->string('company_id')->nullable();
            $table->string('branch_id')->nullable();
            $table->string('created_by')->nullable(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('quotes_master');
    }
};
