<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pos_order_master', function (Blueprint $table) {
            $table->id();
            $table->string('order_no');
            $table->string('order_ref')->nullable();
            $table->timestamp('order_date');
            $table->date('order_date_due')->nullable();
            $table->float('total_amount')->nullable();
            $table->float('discount_amount')->nullable();
            $table->string('promo_code_id')->nullable();
            $table->string('promo_code')->nullable();
            $table->float('vat_amount')->nullable();
            $table->float('net_amount')->nullable();
            $table->tinyInteger('type_id')->nullable();
            $table->tinyInteger('status_id')->nullable();
            $table->boolean('is_paid')->default(false);
            $table->boolean('is_returned')->default(false);
            $table->tinyInteger('count_returned')->default(0);
            $table->string('order_tags')->nullable();
            $table->string('customer_id')->nullable();
            $table->string('customer_name')->nullable();
            $table->string('salesman_id')->nullable();
            $table->string('salesman_name')->nullable();
            $table->string('notes')->nullable();
            $table->string('pos_sessions_id')->nullable();
            $table->string('tenant_id')->nullable();
            $table->string('company_id')->nullable();
            $table->string('branch_id')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pos_order_master');
    }
};
