<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pos_order_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id');
            $table->string('order_no');
            $table->string('product_id');
            $table->string('product_name');
            $table->string('product_desc');
            $table->boolean('is_returned')->default(false);
            $table->integer('unit_id')->default(0);
            $table->float('qty');
            $table->tinyInteger('uom')->nullable();
            $table->tinyInteger('uom_template_id')->nullable();
            $table->float('price');
            $table->float('total_price');
            $table->float('product_discount')->nullable();
            $table->float('product_net_total')->nullable();
            $table->float('product_vat')->nullable();
            $table->float('product_net_total_with_vat')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pos_order_detail');
    }
};
