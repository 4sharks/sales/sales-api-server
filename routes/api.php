<?php

use App\Http\Controllers\Inv\InvController;
use App\Http\Controllers\Inv\InvReturnsController;
use App\Http\Controllers\Inv\PaymentMethodController;
use App\Http\Controllers\Inv\PaymentTransactionController;
use App\Http\Controllers\Inv\ZatcaController;
use App\Http\Controllers\ListPriceController;
use App\Http\Controllers\ListPriceItemsController;
use App\Http\Controllers\PosOrderDetailController;
use App\Http\Controllers\PosOrderMasterController;
use App\Http\Controllers\PosSessionController;
use App\Http\Controllers\Quote\QuoteController;
use App\Http\Controllers\UomController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Quotes Routes
Route::get('/sales/quotes/{tenant_id}/{company_id}/{branch_id}',[QuoteController::class, 'index']);
Route::post('/sales/quotes',[QuoteController::class, 'store']);
Route::get('/sales/quotes/show/{id}',[QuoteController::class, 'show']);
Route::get('/sales/quotes/{tenant_id}/{company_id}/{branch_id}/{inv_no}',[QuoteController::class, 'getByInvNo']);
Route::put('/sales/quotes/update/{id}',[QuoteController::class, 'update']);
Route::delete('/sales/quotes/delete/{id}',[QuoteController::class, 'destroy']);
Route::post('/sales/quotes/get-last-inv-no-by-tenant',[QuoteController::class, 'get_last_invno_by_tenant']);
Route::post('/sales/quotes/get-last-inv-no-by-tenant-with-prefix',[QuoteController::class, 'get_last_invno_by_tenant_with_prefix']);
Route::get('/sales/quotes/customers/{tenant_id}/{company_id}/{branch_id}/{customer_id}',[QuoteController::class, 'getByCustomer']);

Route::get('/sales/quotes/month/{tenant_id}/{company_id}/{branch_id}/{month}',[QuoteController::class, 'sumInvoicesByMonth']);
Route::get('/sales/quotes/year/{tenant_id}/{company_id}/{branch_id}/{year}',[QuoteController::class, 'sumInvoicesByYear']);
Route::get('/sales/quotes/paid/{tenant_id}/{company_id}/{branch_id}/{year}/{is_paid}',[QuoteController::class, 'sumInvoicesPaidByYear']);
Route::get('/sales/quotes/month-group/{tenant_id}/{company_id}/{branch_id}/{year}',[QuoteController::class, 'sumGroupByMonth']);
Route::get('/sales/quotes/salesman/{tenant_id}/{company_id}/{branch_id}/{year}',[QuoteController::class, 'sumBySalesman']);


// Invoice Routes
Route::get('/sales/invoices/{tenant_id}/{company_id}/{branch_id}/{with_zatca?}/{zatca_inv_type?}/{zatca_status?}',[InvController::class, 'get_all']);
Route::post('/sales/invoices',[InvController::class, 'store']);
Route::get('/sales/invoices/show/{id}',[InvController::class, 'show']);
Route::get('/sales/invoices/{tenant_id}/{company_id}/{branch_id}/{inv_no}',[InvController::class, 'getByInvNo']);
Route::put('/sales/invoices/update/{id}',[InvController::class, 'update']);
Route::delete('/sales/invoices/delete/{id}',[InvController::class, 'destroy']);
Route::post('/sales/invoices/get-last-inv-no-by-tenant',[InvController::class, 'get_last_invno_by_tenant']);
Route::post('/sales/invoices/get-last-inv-no-by-tenant-with-prefix',[InvController::class, 'get_last_invno_by_tenant_with_prefix']);
Route::get('/sales/invoices/customers/{tenant_id}/{company_id}/{branch_id}/{customer_id}',[InvController::class, 'getByCustomer']);
Route::post('/sales/invoices/set-paid/{id}',[InvController::class, 'setPaid']);

// Zatca
Route::post('/sales/invoices/Zatca',[ZatcaController::class, 'store']);
Route::get('/sales/invoices/zatca-last/{tenant_id}',[ZatcaController::class, 'get_last_zatca']);


Route::get('/sales/invoices-dashboard/month/{tenant_id}/{company_id}/{branch_id}/{month}',[InvController::class, 'sumInvoicesByMonth']);
Route::get('/sales/invoices-dashboard/year/{tenant_id}/{company_id}/{branch_id}/{year}',[InvController::class, 'sumInvoicesByYear']);
Route::get('/sales/invoices-dashboard/paid/{tenant_id}/{company_id}/{branch_id}/{year}/{is_paid}',[InvController::class, 'sumInvoicesPaidByYear']);
Route::get('/sales/invoices-dashboard/month-group/{tenant_id}/{company_id}/{branch_id}/{year}',[InvController::class, 'sumGroupByMonth']);
Route::get('/sales/invoices-dashboard/salesman/{tenant_id}/{company_id}/{branch_id}/{year}',[InvController::class, 'sumBySalesman']);
Route::get('/sales/invoices-dashboard/topProducts/{tenant_id}/{company_id}/{branch_id}/{year}',[InvController::class, 'topProducts']);
Route::post('/sales/invoices/filterBy',[InvController::class, 'getFilterBy']);

// Invoice Returns Routes
Route::get('/sales/invoices-returns/{tenant_id}/{company_id}/{branch_id}',[InvReturnsController::class, 'index']);
Route::post('/sales/invoices-returns',[InvReturnsController::class, 'store']);
Route::get('/sales/invoices-returns/show/{id}',[InvReturnsController::class, 'show']);
Route::get('/sales/invoices-returns/{tenant_id}/{company_id}/{branch_id}/{inv_no}',[InvReturnsController::class, 'getByInvNo']);
Route::put('/sales/invoices/update/{id}',[InvReturnsController::class, 'update']);
Route::delete('/sales/invoices-returns/delete/{id}',[InvReturnsController::class, 'destroy']);
Route::post('/sales/invoices-returns/get-last-inv-no-by-tenant',[InvReturnsController::class, 'get_last_invno_by_tenant']);
Route::post('/sales/invoices-returns/get-last-inv-no-by-tenant-with-prefix',[InvReturnsController::class, 'get_last_invno_by_tenant_with_prefix']);
Route::get('/sales/invoices-returns/customers/{tenant_id}/{company_id}/{branch_id}/{customer_id}',[InvReturnsController::class, 'getByCustomer']);
Route::post('/sales/invoices-returns/set-paid/{id}',[InvReturnsController::class, 'setPaid']);

Route::get('/sales/invoices-returns/month/{tenant_id}/{company_id}/{branch_id}/{month}',[InvReturnsController::class, 'sumInvoicesByMonth']);
Route::get('/sales/invoices-returns/year/{tenant_id}/{company_id}/{branch_id}/{year}',[InvReturnsController::class, 'sumInvoicesByYear']);
Route::get('/sales/invoices-returns/paid/{tenant_id}/{company_id}/{branch_id}/{year}/{is_paid}',[InvReturnsController::class, 'sumInvoicesPaidByYear']);
Route::get('/sales/invoices-returns/month-group/{tenant_id}/{company_id}/{branch_id}/{year}',[InvReturnsController::class, 'sumGroupByMonth']);
Route::get('/sales/invoices-returns/salesman/{tenant_id}/{company_id}/{branch_id}/{year}',[InvReturnsController::class, 'sumBySalesman']);
Route::get('/sales/invoices-returns/topProducts/{tenant_id}/{company_id}/{branch_id}/{year}',[InvReturnsController::class, 'topProducts']);
Route::post('/sales/invoices-returns/filterBy',[InvReturnsController::class, 'getFilterBy']);

// Payment Methods Routes
Route::get('/sales/payment-methods/{tenant_id}/{company_id}/{branch_id}',[PaymentMethodController::class,'index']);
Route::post('/sales/payment-methods',[PaymentMethodController::class,'store']);
Route::put('/sales/payment-methods/update/{id}',[PaymentMethodController::class,'update']);
Route::delete('/sales/payment-methods/delete/{id}',[PaymentMethodController::class,'destroy']);

// Payment Transactions Routes
Route::get('/sales/payment-transactions/{tenant_id}/{company_id}/{branch_id}',[PaymentTransactionController::class,'index']);
Route::get('/sales/payment-transactions/invoice/{inv_id}',[PaymentTransactionController::class,'getTrannsactionsByInvId']);
Route::post('/sales/payment-transactions',[PaymentTransactionController::class,'store']);
Route::post('/sales/payment-transactions/filter',[PaymentTransactionController::class,'filterBy']);
Route::post('/sales/payment-transactions-multi',[PaymentTransactionController::class,'store_multi']);


// UOM Inventory
Route::post('/sales/inventory/uom',[UomController::class,'store']);
Route::get('/sales/inventory/uom/tenant/{tenant_id}',[UomController::class,'get_by_tenant']);
Route::get('/sales/inventory/uom/{id}',[UomController::class,'get_by_id']);
Route::put('/sales/inventory/uom',[UomController::class,'update']);
Route::delete('/sales/inventory/uom/{id}',[UomController::class,'delete']);
Route::delete('/sales/inventory/uom/details/{id}',[UomController::class,'delete_uom_detail']);

// List PRICE
Route::get('/sales/inventory/list-price/{tenant_id}/{company_id}/{branch_id}',[ListPriceController::class,'index']);
Route::get('/sales/inventory/list-price/{id}',[ListPriceController::class,'get_list']);
Route::post('/sales/inventory/list-price',[ListPriceController::class,'store']);
Route::put('/sales/inventory/list-price/{id}',[ListPriceController::class,'update']);
Route::delete('/sales/inventory/list-price/{id}',[ListPriceController::class,'delete']);

// List PRICE ITEMS
Route::get('/sales/inventory/list-price-items/{tenant_id}/{company_id}/{branch_id}',[ListPriceItemsController::class,'index']);
Route::get('/sales/inventory/list-price-items/{list_price_id}',[ListPriceItemsController::class,'get_list_items']);
Route::post('/sales/inventory/list-price-items',[ListPriceItemsController::class,'store']);
Route::put('/sales/inventory/list-price-items/{id}',[ListPriceItemsController::class,'update']);
Route::delete('/sales/inventory/list-price-items/{id}',[ListPriceItemsController::class,'delete']);
Route::delete('/sales/inventory/list-price-items',[ListPriceItemsController::class,'delete_all']);


// POS Routes
Route::get('/sales/pos/orders/{tenant_id}/{company_id}/{branch_id}/{created_by}',[PosOrderMasterController::class,'create']);
Route::get('/sales/pos/orders/open/{tenant_id}/{company_id}/{branch_id}/{created_by}',[PosOrderMasterController::class,'get_open_orders_by_user']);
Route::get('/sales/pos/orders/{order_id}',[PosOrderMasterController::class,'get_order']);
Route::post('/sales/pos/orders/detail',[PosOrderDetailController::class,'add_item']);
Route::put('/sales/pos/orders/detail/qty/{id}/{qty}',[PosOrderDetailController::class,'update_qty']);
Route::put('/sales/pos/orders/detail/{id}',[PosOrderDetailController::class,'update_item']);
Route::delete('/sales/pos/orders/detail/{id}',[PosOrderDetailController::class,'delete']);
// POS Sessions
Route::get('/sales/pos/sessions/gen-code/{tenant_id}/{company_id}/{branch_id}',[PosSessionController::class,'gen_session_code']);
Route::post('/sales/pos/sessions/open',[PosSessionController::class,'create_session']);
Route::put('/sales/pos/sessions/close/{session_id}',[PosSessionController::class,'close_session']);
